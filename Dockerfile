FROM gradle:6.7.0-jdk8

MAINTAINER Daniel Sturm <mail@danielsturm.de>

RUN set -x \
 && apt-get -yq update \
 && apt-get -yq install graphviz
