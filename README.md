# Gradle Graphviz Docker

This is a docker container to run a [gradle](https://gradle.org/) build with [graphviz](https://graphviz.org/) support.